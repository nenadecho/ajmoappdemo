//
//  CustomPick.swift
//  AjmoAppDemo
//
//  Created by Nenad Ljubik on 10/8/20.
//  Copyright © 2020 Nenad Ljubik. All rights reserved.
//

import Foundation

class CustomPick: Codable {
    
    var id: Int?
    var order: Int?
    var tags: [Tag]?
    var type: String?
    var items: [Venue]?
    var title: String?
}
