//
//  AllTags.swift
//  AjmoAppDemo
//
//  Created by Nenad Ljubik on 10/8/20.
//  Copyright © 2020 Nenad Ljubik. All rights reserved.
//

import Foundation

class AllTags: Codable {
    
    var primaryTags: [Tag]?
    var secondaryTags: [Tag]?
}
