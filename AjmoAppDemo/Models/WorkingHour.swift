//
//  WorkingHour.swift
//  AjmoAppDemo
//
//  Created by Nenad Ljubik on 10/8/20.
//  Copyright © 2020 Nenad Ljubik. All rights reserved.
//

import Foundation

class WorkingHour: Codable {
    
    var id: Int?
    var venue_id: Int?
    var day: Int?
    var start: String?
    var end: String?
}
