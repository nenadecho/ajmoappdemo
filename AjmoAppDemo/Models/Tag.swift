//
//  Tag.swift
//  AjmoAppDemo
//
//  Created by Nenad Ljubik on 10/8/20.
//  Copyright © 2020 Nenad Ljubik. All rights reserved.
//

import Foundation

class Tag: Codable {
    
    var id: Int?
    var color: String?
    var tag_group_id: Int?
    var name: String?
}
