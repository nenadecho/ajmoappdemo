//
//  Venue.swift
//  AjmoAppDemo
//
//  Created by Nenad Ljubik on 10/8/20.
//  Copyright © 2020 Nenad Ljubik. All rights reserved.
//

import Foundation

class Venue: Codable {
    
    var id: Int?
    var subtitle: String?
    var picture_url: String?
    var venue_categories: [VenueCategory]?
    var allTags: AllTags?
    var primary_tag_group: String?
    var address: String?
    var opened: Bool?
    var web: String?
    var telephone: String?
    var share_link: String?
    var city: String?
    var lat: Double?
    var lon: Double?
    var trending: Int?
    var promoted: Int?
    var bat: Int?
    var has_qr_code: Int?
    var active: Int?
    var smoking_area: Int?
    var working_hours: [WorkingHour]?
    var gallery: [Gallery]?
    var type: String?
    var name: String?
    var description: String?
}
