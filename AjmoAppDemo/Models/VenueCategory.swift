//
//  VenueCategory.swift
//  AjmoAppDemo
//
//  Created by Nenad Ljubik on 10/8/20.
//  Copyright © 2020 Nenad Ljubik. All rights reserved.
//

import Foundation

class VenueCategory: Codable {
    
    var id: Int?
    var image: String?
    var name: String?
    var feeling_lucky_title: String?
}
