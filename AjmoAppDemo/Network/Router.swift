//
//  Router.swift
//  AjmoAppDemo
//
//  Created by Nenad Ljubik on 10/8/20.
//  Copyright © 2020 Nenad Ljubik. All rights reserved.
//

import Foundation
import Alamofire


enum Router: URLRequestConvertible {
    case GetVenue
    
    var method: Alamofire.HTTPMethod {
        switch self {
        default:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .GetVenue:
            return NetworkConstants.Network.Endpoints.venue
        default:
            return ""
        }
    }
    
    var parameters: [String:Any] {
        switch self {
        default:
            return [:]
        }
    }
    
    var header: [String:String] {
        return NetworkConstants.Network.header
    }
    
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest : URLRequest!
        var url: URL!
        url = URL(string: NetworkConstants.Network.baseURL + path)
        
        urlRequest = URLRequest(url: url!)
        urlRequest.httpMethod = method.rawValue
        print(header)
        if method.rawValue != "GET" {
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: self.parameters, options: .prettyPrinted)
        }
        urlRequest.allHTTPHeaderFields = header
        
        return urlRequest
    }
}
