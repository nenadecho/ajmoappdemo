//
//  Network.swift
//  AjmoAppDemo
//
//  Created by Nenad Ljubik on 10/8/20.
//  Copyright © 2020 Nenad Ljubik. All rights reserved.
//

import Foundation


struct NetworkConstants {
    
    struct Network {
        
        static let header = ["Content-Type": "application/json"]
        
        static let baseURL = "https://api.ajmo.hr/v3"
        
        struct Endpoints {
            static let venue = "/venue/dashboard"
        }
    }
}
