//
//  ApiManager.swift
//  AjmoAppDemo
//
//  Created by Nenad Ljubik on 10/8/20.
//  Copyright © 2020 Nenad Ljubik. All rights reserved.
//

import Foundation
import Alamofire

typealias CompletitionCallBack = ((_ success: Bool, _ responseObject: [String:Any]?,_ statusCode : Int?)-> ())?

class ApiManager {
    
    static let sharedInstance = ApiManager()
    
    //MARK:- Main Function For Executing A Request
    private func executeRequest(request : URLRequestConvertible,completitionCallback : CompletitionCallBack) {
        AF.request(request).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                let json = value as? [String:Any]

                var statusCode = 200
                if let res = response.response {
                    statusCode = res.statusCode
                    if statusCode == 200 {
                        completitionCallback!(true,json,statusCode)
                    } else {
                        completitionCallback!(false,json,statusCode)
                    }
                }
            case .failure(_):
                if let res = response.response {
                    let statusCode = res.statusCode
                    completitionCallback!(false,nil,statusCode)
                } else {
                    completitionCallback!(false,nil,nil)
                }
            }
        }
    }
    
    //MARK:- Get Venues
    func getVenues(completition: CompletitionCallBack) {
        executeRequest(request: Router.GetVenue, completitionCallback: completition)
    }
}
