//
//  Utilities.swift
//  AjmoAppDemo
//
//  Created by Nenad Ljubik on 10/7/20.
//  Copyright © 2020 Nenad Ljubik. All rights reserved.
//

import Foundation
import UIKit

class Utilities {
    
    static let sharedInstance = Utilities()
    
    let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
    var trendingVenues = [Venue]()
    var coffeeVenues = [Venue]()
    var liveGigsVenues = [Venue]()
    
    //MARK:- Create Label
    func createLabelWith(text: String, txtAlignment: NSTextAlignment, font: UIFont, textColor: UIColor, backgroundColor: UIColor) -> UILabel {
        let label = UILabel()
        label.text = text
        label.font = font
        label.textColor = textColor
        label.backgroundColor = backgroundColor
        label.textAlignment = txtAlignment
        label.numberOfLines = 0
        
        return label
    }
    
    //MARK:- Create ImageView
    func createImageViewWith(imageName: String, contentMode: UIView.ContentMode, backgroundColor: UIColor, cornerRadius: CGFloat) -> UIImageView {
        let imageView = UIImageView()
        if (imageName != "") {
            imageView.image = UIImage(named: imageName)
        }
        imageView.backgroundColor = backgroundColor
        imageView.contentMode = contentMode
        imageView.layer.cornerRadius = cornerRadius
        
        return imageView
    }
    
    //MARK:- Convert Hex to UIColor
    func colorFromHexCode(hex: String) -> UIColor {
        let r, g, b, a: CGFloat
        
        var hexColor = hex
        
        if hex.hasPrefix("#") {
            hexColor = hex.trimmingCharacters(in: .whitespacesAndNewlines)
            hexColor = hexColor.replacingOccurrences(of: "#", with: "")
        }
        
        if hexColor.count == 8 {
            let scanner = Scanner(string: hexColor)
            var hexNumber: UInt64 = 0
            
            if scanner.scanHexInt64(&hexNumber) {
                r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                a = CGFloat(hexNumber & 0x000000ff) / 255
                
                return UIColor(red: r, green: g, blue: b, alpha: a)
            }
        } else if (hexColor.count == 6) {
            let scanner = Scanner(string: hexColor)
            var hexNumber: UInt64 = 0
            
            if scanner.scanHexInt64(&hexNumber) {
                r = CGFloat((hexNumber & 0xFF0000) >> 16) / 255.0
                g = CGFloat((hexNumber & 0x00FF00) >> 8) / 255.0
                b = CGFloat(hexNumber & 0x0000FF) / 255.0
                
                return UIColor(red: r, green: g, blue: b, alpha: 1.0)
            }
        }
        return .lightGray
    }
    
    //MARK:- Converting JSON To Data
    func jsonToData(json: Any) -> Data? {
        do {
            return try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
        } catch {
            print(error.localizedDescription)
        }
        return nil
    }
    
    //MARK:- Filter Venues According Selected Tag
    func filterVenuesAccordingSelectedTag(venues: [Venue], tagName: String) -> [Venue] {
        var filteredVenues = [Venue]()
        
        for venue in venues {
            if let venueTags = venue.allTags, let primaryTags = venueTags.primaryTags, let secondaryTags = venueTags.secondaryTags {
                for primaryTag in primaryTags {
                    if let name = primaryTag.name {
                        if (name == tagName) {
                            filteredVenues.append(venue)
                        }
                    }
                }
                
                for secondaryTag in secondaryTags {
                    if let name = secondaryTag.name {
                        if (name == tagName) {
                            filteredVenues.append(venue)
                        }
                    }
                }
            }
        }
        
        return filteredVenues
    }
    
    //MARK:- Merge Venues
    private func mergeVenues(firstVenues: [Venue], secondVenues: [Venue]) -> [Venue] {
        var venues = [Venue]()
        
        for secondVenue in secondVenues {
            let contains = firstVenues.contains { (venue) -> Bool in
                if let venueID = venue.id, let secondVenueID = secondVenue.id {
                    if (venueID == secondVenueID) {
                        return true
                    } else {
                        return false
                    }
                } else {
                    return false
                }
            }
            
            if (!contains) {
                venues.append(secondVenue)
            }
        }
        
        
        return venues
    }
    
    //MARK:- Get Sorted And Filtered Venus
    func getSortedAndFilteredVenues(tagName: String) -> [Venue]{
        let filteredCoffeeVenues = filterVenuesAccordingSelectedTag(venues: coffeeVenues, tagName: tagName)
        let filteredLiveGigsVenues = filterVenuesAccordingSelectedTag(venues: liveGigsVenues, tagName: tagName)
        let filteredTrendingVenues = filterVenuesAccordingSelectedTag(venues: trendingVenues, tagName: tagName)
        
        var filteredVenues = [Venue]()
        
        filteredVenues.append(contentsOf: filteredCoffeeVenues)
        
        filteredVenues.append(contentsOf: mergeVenues(firstVenues: filteredVenues, secondVenues: filteredLiveGigsVenues))
        filteredVenues.append(contentsOf: mergeVenues(firstVenues: filteredVenues, secondVenues: filteredTrendingVenues))
        
        filteredVenues.sort { (venue1, venue2) -> Bool in
            if let name1 = venue1.name, let name2 = venue2.name {
                if (name1 < name2) {
                    return true
                } else {
                    return false
                }
            } else {
                return false
            }
        }
        
        return filteredVenues
    }
    
    //MARK: - Check For Internet Connection
    func checkForInternetConnection() -> Bool {
        return Reachability.sharedInstance.isInternetAvailable()
    }
    
    //MARK: - Create Alert For No Internet Connection
    func createAlertForNoInternetConnection(controller: UIViewController) {
        let alert = UIAlertController(title: "The Internet Connection Seems To Be Offline", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        controller.present(alert,animated: true)
    }
    
    //MARK: - CREATE ALERT WITH TITLE AND OK ACTION
    func createAlert(title: String, message: String, controller: UIViewController) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        controller.present(alert,animated: true)
    }
    
    //MARK: - Showing/Hiding Activity indicator
    func shouldShowIndicator(view: UIView, shouldAnimate: Bool){
        DispatchQueue.main.async {
            self.activityIndicator.color = .lightGray
            view.addSubview(self.activityIndicator)
            self.activityIndicator.snp.makeConstraints { (make) in
                make.center.equalTo(view)
            }
            
            if(shouldAnimate){
                self.activityIndicator.startAnimating()
            } else {
                self.activityIndicator.stopAnimating()
            }
        }
    }
    
    //MARK: - Create Button
    func createButtonWith(title: String, titleColor: UIColor, font: UIFont, corner: CGFloat, backgroundColor: UIColor, tag: Int) -> UIButton {
        let button = UIButton()
        button.setTitle(title, for: .normal)
        button.setTitleColor(titleColor, for: .normal)
        
        button.titleLabel?.font = font
        button.layer.cornerRadius = corner
        button.backgroundColor = backgroundColor
        button.tag = tag
        
        return button
    }
    
    //MARK:- Create Image For Gradient NavigationBar
    func getImageFrom(gradientLayer:CAGradientLayer) -> UIImage? {
        var gradientImage:UIImage?
        UIGraphicsBeginImageContext(gradientLayer.frame.size)
        if let context = UIGraphicsGetCurrentContext() {
            gradientLayer.render(in: context)
            gradientImage = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch)
        }
        UIGraphicsEndImageContext()
        return gradientImage
    }
}
