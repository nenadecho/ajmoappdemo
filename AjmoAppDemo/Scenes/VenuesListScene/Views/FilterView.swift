//
//  FilterView.swift
//  AjmoAppDemo
//
//  Created by Nenad Ljubik on 10/8/20.
//  Copyright © 2020 Nenad Ljubik. All rights reserved.
//

import UIKit

class FilterView: UIView {
    
    //UI
    var filtersLabel: UILabel!
    var tagLabel: UILabel!
    var lineView: UIView!
    
    
    //MARK:- View Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- Setting Up Views
    func setupViews() {
        
        filtersLabel = Utilities.sharedInstance.createLabelWith(text: "Filters: ", txtAlignment: .left, font: .systemFont(ofSize: 14), textColor: .gray, backgroundColor: .clear)
        
        tagLabel = Utilities.sharedInstance.createLabelWith(text: "casual", txtAlignment: .left, font: .boldSystemFont(ofSize: 14), textColor: .gray, backgroundColor: .clear)
        
        lineView = UIView()
        lineView.backgroundColor = .lightGray
        
        self.addSubview(filtersLabel)
        self.addSubview(tagLabel)
        self.addSubview(lineView)
    }
    
    //MARK:- Setting Up Views
    func setupConstraints() {
        filtersLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self).offset(30)
            make.centerY.equalTo(self)
        }
        
        tagLabel.snp.makeConstraints { (make) in
            make.left.equalTo(filtersLabel.snp.right).offset(10)
            make.centerY.equalTo(self)
        }
        
        lineView.snp.makeConstraints { (make) in
            make.height.equalTo(1)
            make.left.right.bottom.equalTo(self)
        }
    }
}
