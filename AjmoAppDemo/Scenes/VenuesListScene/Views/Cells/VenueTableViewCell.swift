//
//  VenueTableViewCell.swift
//  AjmoAppDemo
//
//  Created by Nenad Ljubik on 10/8/20.
//  Copyright © 2020 Nenad Ljubik. All rights reserved.
//

import UIKit

class VenueTableViewCell: UITableViewCell {
    
    //UI
    var venueImageView: UIImageView!
    var venueNameLabel: UILabel!
    var venueAddressLabel: UILabel!
    var hotPickImageView: UIImageView!
    var hotPickHolderView: UIView!
    
    
    //MARK:- Cell Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        setGradientBackground()
    }
    
    override func prepareForReuse() {
        hotPickImageView.isHidden = true
        hotPickHolderView.isHidden = true
    }
    
    //MARK:- Setting Up Views
    func setupViews() {
        self.selectionStyle = .none
        
        venueImageView = Utilities.sharedInstance.createImageViewWith(imageName: "", contentMode: .scaleAspectFill, backgroundColor: .clear, cornerRadius: 8)
        venueImageView.layer.masksToBounds = true
        
        hotPickHolderView = UIView()
        hotPickHolderView.layer.cornerRadius = 8
        hotPickHolderView.layer.masksToBounds = true
        hotPickHolderView.isHidden = true
        
        hotPickImageView = Utilities.sharedInstance.createImageViewWith(imageName: "flameIcon", contentMode: .scaleAspectFit, backgroundColor: .clear, cornerRadius: 8)
        hotPickImageView.isHidden = true
        hotPickImageView.layer.masksToBounds = true
        
        venueNameLabel = Utilities.sharedInstance.createLabelWith(text: "", txtAlignment: .left, font: .boldSystemFont(ofSize: 14), textColor: .black, backgroundColor: .clear)
        
        
        venueAddressLabel = Utilities.sharedInstance.createLabelWith(text: "", txtAlignment: .left, font: .boldSystemFont(ofSize: 14), textColor: .black, backgroundColor: .clear)
        venueAddressLabel.numberOfLines = 1
        
        self.addSubview(venueImageView)
        self.addSubview(hotPickHolderView)
        self.addSubview(hotPickImageView)
        self.addSubview(venueNameLabel)
        self.addSubview(venueAddressLabel)
    }
    
    //MARK:- Setting Up Constraints
    func setupConstraints() {
        venueImageView.snp.makeConstraints { (make) in
            make.centerY.equalTo(self)
            make.left.equalTo(self).offset(20)
            make.height.equalTo(self.snp.height).dividedBy(1.2)
            make.width.equalTo(self.snp.width).dividedBy(2.5)
        }
        
        hotPickImageView.snp.makeConstraints { (make) in
            make.left.equalTo(venueImageView).offset(-10)
            make.top.equalTo(venueImageView).offset(10)
            make.width.height.equalTo(30)
        }
        
        hotPickHolderView.snp.makeConstraints { (make) in
            make.edges.equalTo(hotPickImageView)
        }
        
        venueNameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(venueImageView).offset(5)
            make.left.equalTo(venueImageView.snp.right).offset(20)
            make.right.equalTo(self).offset(-20)
        }
        
        venueAddressLabel.snp.makeConstraints { (make) in
            make.left.equalTo(venueNameLabel)
            make.centerY.equalTo(self)
            make.right.equalTo(self).offset(-20)
        }
    }
    
    //MARK:- Setup Cell
    func setupCell(venue: Venue) {
        if let image = venue.picture_url {
            venueImageView.kf.setImage(with: URL(string: image))
        }
        
        venueNameLabel.text = venue.name ?? ""
        venueAddressLabel.text = venue.address ?? ""
        
        if let isHotPick = venue.bat {
            if (isHotPick == 1) {
                hotPickImageView.isHidden = false
                hotPickHolderView.isHidden = false
            } else {
                hotPickImageView.isHidden = true
                hotPickHolderView.isHidden = true
            }
        }
    }
    
    //MARK:- Setting Gradient Background
    func setGradientBackground() {
        let gradientLayer: CAGradientLayer = CAGradientLayer()
        gradientLayer.frame = hotPickHolderView.bounds
        
        let topColor: CGColor = UIColor(red: 0.90, green: 0.44, blue: 0.22, alpha: 1.00).cgColor
        let bottomColor: CGColor = UIColor(red: 0.84, green: 0.34, blue: 0.27, alpha: 1.00).cgColor
        
        gradientLayer.colors = [topColor, bottomColor]
        
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        hotPickHolderView.layer.insertSublayer(gradientLayer, at: 0)
    }
}

