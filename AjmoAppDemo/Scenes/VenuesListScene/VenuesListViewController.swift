//
//  VenuesListViewController.swift
//  AjmoAppDemo
//
//  Created by Nenad Ljubik on 10/8/20.
//  Copyright © 2020 Nenad Ljubik. All rights reserved.
//

import UIKit

class VenuesListViewController: UIViewController {
    
    //UI
    var filterView: FilterView!
    var tableView: UITableView!
    
    //Helpers
    var venues = [Venue]()
    var tagName: String!
    
    
    //MARK:- Controller's Initialization
    init(venues: [Venue], tagName: String) {
        super.init(nibName: nil, bundle: nil)
        self.venues = venues
        self.tagName = tagName
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- Controller's Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        setupConstraints()
    }
    
    
    //MARK:- Setting Up Views
    func setupViews() {
        self.view.backgroundColor = .white
        
        filterView = FilterView()
        filterView.tagLabel.text = tagName
        
        tableView = UITableView()
        tableView.register(VenueTableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.delegate = self
        tableView.dataSource = self
        
        self.view.addSubview(filterView)
        self.view.addSubview(tableView)
    }
    
    //MARK:- Setting Up Views
    func setupConstraints() {
        filterView.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(self.view.safeAreaLayoutGuide)
            make.height.equalTo(50)
        }
        
        tableView.snp.makeConstraints { (make) in
            make.left.bottom.right.equalTo(self.view)
            make.top.equalTo(filterView.snp.bottom)
        }
    }
}


//MARK:- UITableView Delegate And DataSource Methods
extension VenuesListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return venues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! VenueTableViewCell
        cell.setupCell(venue: venues[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        navigationController?.pushViewController(VenueDetailsViewController(venue: venues[indexPath.row]), animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.size.width / 3
    }
}
