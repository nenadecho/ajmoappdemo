//
//  VenueCollectionViewCell.swift
//  AjmoAppDemo
//
//  Created by Nenad Ljubik on 10/8/20.
//  Copyright © 2020 Nenad Ljubik. All rights reserved.
//

import UIKit
import Kingfisher

class VenueCollectionViewCell: UICollectionViewCell {
    
    //UI
    var holderView: UIView!
    var hotPickView: HotPickView!
    var venueImageView: UIImageView!
    var venueNameLabel: UILabel!
    var venueCategoryLabel: UILabel!
    
    //MARK:- Cell Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- Setting Up Views
    func setupViews() {
    
        holderView = UIView()
        holderView.backgroundColor = .white
        holderView.layer.cornerRadius = 8
        holderView.clipsToBounds = true
        
        hotPickView = HotPickView()
        
        venueImageView = Utilities.sharedInstance.createImageViewWith(imageName: "", contentMode: .scaleAspectFill, backgroundColor: .clear, cornerRadius: 0)
        venueImageView.layer.masksToBounds = true
        
        venueNameLabel = Utilities.sharedInstance.createLabelWith(text: "", txtAlignment: .left, font: .systemFont(ofSize: 18), textColor: .black, backgroundColor: .clear)
        
        venueCategoryLabel = Utilities.sharedInstance.createLabelWith(text: "", txtAlignment: .left, font: .systemFont(ofSize: 14), textColor: .gray, backgroundColor: .clear)
        
        self.contentView.addSubview(holderView)
        self.contentView.addSubview(hotPickView)
        
        holderView.addSubview(venueImageView)
        holderView.addSubview(venueNameLabel)
        holderView.addSubview(venueCategoryLabel)
    }
    
    //MARK:- Setting Up Constraints
    func setupConstraints() {
        holderView.snp.makeConstraints { (make) in
            make.top.bottom.right.equalTo(self.contentView)
            make.left.equalTo(self.contentView).offset(20)
        }
        
        hotPickView.snp.makeConstraints { (make) in
            make.left.equalTo(holderView).offset(-10)
            make.top.equalTo(holderView).offset(10)
            make.height.equalTo(40)
            make.width.equalTo(110)
        }
        
        venueCategoryLabel.snp.makeConstraints { (make) in
            make.bottom.equalTo(holderView).offset(-10)
            make.left.equalTo(holderView).offset(20)
            make.right.equalTo(holderView).offset(-20)
        }
        
        venueNameLabel.snp.makeConstraints { (make) in
            make.bottom.equalTo(venueCategoryLabel.snp.top).offset(-10)
            make.left.right.equalTo(venueCategoryLabel)
        }
        
        venueImageView.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(holderView)
            make.bottom.equalTo(venueNameLabel.snp.top).offset(-5)
        }
    }
    
    //MARK:- Setup Cell
    func setupCell(venue: Venue) {
        if let image = venue.picture_url {
            venueImageView.kf.setImage(with: URL(string: image))
        }
        
        venueNameLabel.text = venue.name ?? ""
        venueCategoryLabel.text = venue.subtitle ?? ""
    }
}
