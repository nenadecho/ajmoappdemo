//
//  DiscoverTableViewCell.swift
//  AjmoAppDemo
//
//  Created by Nenad Ljubik on 10/7/20.
//  Copyright © 2020 Nenad Ljubik. All rights reserved.
//

import UIKit

protocol DiscoverTableViewCellDelegate {
    func tagWasTapped(tagName: String)
    func venueWasTapped(venue: Venue)
}

class DiscoverTableViewCell: UITableViewCell {
    
    //UI
    var venuesCollectionView: UICollectionView!
    var tagsCollectionView: UICollectionView!
    var venueLayout: UICollectionViewFlowLayout!
    var tagLayout: UICollectionViewFlowLayout!
    
    //Helpers
    var venues = [Venue]()
    var tags = [Tag]()
    var delegate: DiscoverTableViewCellDelegate!
    var indexOfCellBeforeDragging = 0
    
    //MARK:- Cell Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        setCollectionViewFlowLayoutSize()
    }
    
    //MARK:- Setting Up Views
    func setupViews() {
        self.backgroundColor = .clear
        self.selectionStyle = .none
        
        venueLayout = UICollectionViewFlowLayout()
        venueLayout.scrollDirection = .horizontal
        venueLayout.minimumLineSpacing = 0
        
        tagLayout = UICollectionViewFlowLayout()
        tagLayout.scrollDirection = .horizontal
        
        venuesCollectionView = UICollectionView(frame: .zero, collectionViewLayout: venueLayout)
        venuesCollectionView.backgroundColor = .clear
        venuesCollectionView.isPagingEnabled = false
        venuesCollectionView.showsHorizontalScrollIndicator = false
        venuesCollectionView.showsVerticalScrollIndicator = false
        venuesCollectionView.register(VenueCollectionViewCell.self, forCellWithReuseIdentifier: "venueCell")
        venuesCollectionView.delegate = self
        venuesCollectionView.dataSource = self
        
        tagsCollectionView = UICollectionView(frame: .zero, collectionViewLayout: tagLayout)
        tagsCollectionView.backgroundColor = .clear
        tagsCollectionView.showsHorizontalScrollIndicator = false
        tagsCollectionView.showsVerticalScrollIndicator = false
        tagsCollectionView.register(TagCollectionViewCell.self, forCellWithReuseIdentifier: "tagCell")
        tagsCollectionView.delegate = self
        tagsCollectionView.dataSource = self
        
        self.contentView.addSubview(venuesCollectionView)
        self.contentView.addSubview(tagsCollectionView)
    }
    
    //MARK:- Setting Up Constraints
    func setupConstraints() {
        venuesCollectionView.snp.makeConstraints { (make) in
            make.left.right.top.equalTo(self.contentView)
            make.height.equalTo(self.contentView).dividedBy(1.3)
        }
        
        tagsCollectionView.snp.makeConstraints { (make) in
            make.left.right.equalTo(self.contentView)
            make.bottom.equalTo(self.contentView).offset(-10)
            make.top.equalTo(venuesCollectionView.snp.bottom).offset(10)
        }
    }
    
    //MARK:- Setup Cell
    func setupCell(customPick: CustomPick) {
        if let venues = customPick.items, let tags = customPick.tags {
            self.venues = venues
            self.tags = tags
        }
    }
    
    //MARK:- Setting Layout Size And SectionInset
    func setCollectionViewFlowLayoutSize() {
        let inset: CGFloat = 20
        
        venueLayout.sectionInset = UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
        venueLayout.itemSize = CGSize(width: self.contentView.frame.size.width - inset * 2, height: self.contentView.frame.size.height / 1.3)
        
        tagLayout.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        tagLayout.itemSize = CGSize(width: self.frame.size.width / 2 - 40, height: 50)
    }
    
    //MARK:- Getting The Index Of The Most Visible Cell
    func getIndexOfMostVisibleCell() -> Int {
        let itemWidth = venueLayout.itemSize.width
        let proportionalOffset = venueLayout.collectionView!.contentOffset.x / itemWidth
        let index = Int(round(proportionalOffset))
        let safeIndex = max(0, min(venues.count - 1, index))
        return safeIndex
    }
}

//MARK:- UIScrollView Delegate Methods
extension DiscoverTableViewCell {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if (scrollView == tagsCollectionView) {
            return
        }
        
        indexOfCellBeforeDragging = getIndexOfMostVisibleCell()
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        if (scrollView == tagsCollectionView) {
            return
        }
        // Stop scrollView sliding:
        targetContentOffset.pointee = scrollView.contentOffset
        
        let indexOfMostVisibleCell = self.getIndexOfMostVisibleCell()
        
        let swipeVelocityThreshold: CGFloat = 0.5
        let hasEnoughVelocityToSlideToTheNextCell = indexOfCellBeforeDragging + 1 < venues.count && velocity.x > swipeVelocityThreshold
        let hasEnoughVelocityToSlideToThePreviousCell = indexOfCellBeforeDragging - 1 >= 0 && velocity.x < -swipeVelocityThreshold
        let majorCellIsTheCellBeforeDragging = indexOfMostVisibleCell == indexOfCellBeforeDragging
        let didUseSwipeToSkipCell = majorCellIsTheCellBeforeDragging && (hasEnoughVelocityToSlideToTheNextCell || hasEnoughVelocityToSlideToThePreviousCell)
        
        if didUseSwipeToSkipCell {
            let snapToIndex = indexOfCellBeforeDragging + (hasEnoughVelocityToSlideToTheNextCell ? 1 : -1)
            let toValue = venueLayout.itemSize.width * CGFloat(snapToIndex)
            
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: velocity.x, options: .allowUserInteraction, animations: {
                scrollView.contentOffset = CGPoint(x: toValue, y: 0)
                scrollView.layoutIfNeeded()
            }, completion: nil)
            
        } else {
            // This is a much better way to scroll to a cell:
            let indexPath = IndexPath(row: indexOfMostVisibleCell, section: 0)
            venueLayout.collectionView!.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        }
    }
}

//MARK:- UICollectionView Delegate, DataSource And DelegateFlowLayout Methods
extension DiscoverTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (collectionView == venuesCollectionView) {
            return venues.count
        } else {
            return tags.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if (collectionView == venuesCollectionView) {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "venueCell", for: indexPath) as! VenueCollectionViewCell
            cell.setupCell(venue: venues[indexPath.row])
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tagCell", for: indexPath) as! TagCollectionViewCell
            cell.setupCell(tag: tags[indexPath.row])
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (collectionView == tagsCollectionView) {
            guard let tagName = tags[indexPath.row].name else {return}
            delegate.tagWasTapped(tagName: tagName)
        } else {
            delegate.venueWasTapped(venue: venues[indexPath.row])
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
