//
//  TagCollectionViewCell.swift
//  AjmoAppDemo
//
//  Created by Nenad Ljubik on 10/8/20.
//  Copyright © 2020 Nenad Ljubik. All rights reserved.
//

import UIKit

class TagCollectionViewCell: UICollectionViewCell {
    
    //UI
    var tagNameLabel: UILabel!
    
    //MARK:- Cell Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- Setting Up Views
    func setupViews() {
        self.layer.cornerRadius = 8
        
        tagNameLabel = Utilities.sharedInstance.createLabelWith(text: "", txtAlignment: .center, font: .systemFont(ofSize: 14), textColor: .black, backgroundColor: .clear)
        tagNameLabel.numberOfLines = 1
        
        self.addSubview(tagNameLabel)
    }
    
    //MARK:- Setting Up Constraints
    func setupConstraints() {
        tagNameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self).offset(10)
            make.right.equalTo(self).offset(-10)
            make.centerY.equalTo(self)
            make.height.equalTo(40)
        }
    }
    
    //MARK:- Setup Cell
    func setupCell(tag: Tag) {
        tagNameLabel.text = tag.name ?? ""
        
        if let color = tag.color {
            self.backgroundColor = Utilities.sharedInstance.colorFromHexCode(hex: color)
        }
    }

    func setupCellForSecondaryTag(tag: Tag) {
        tagNameLabel.text = tag.name ?? ""
    
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.borderWidth = 1
    }
}
