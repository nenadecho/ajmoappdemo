//
//  HotPickView.swift
//  AjmoAppDemo
//
//  Created by Nenad Ljubik on 10/8/20.
//  Copyright © 2020 Nenad Ljubik. All rights reserved.
//

import UIKit

class HotPickView: UIView {
    
    //UI
    var hotPickImageView: UIImageView!
    var hotPickLabel: UILabel!
    
    
    //MARK:- View Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        setGradientBackground()
    }
    
    //MARK:- Setting Up Views
    func setupViews() {
        
        self.backgroundColor = .cyan
        self.layer.cornerRadius = 8
        self.clipsToBounds = true
        
        hotPickImageView = Utilities.sharedInstance.createImageViewWith(imageName: "flameIcon", contentMode: .scaleAspectFit, backgroundColor: .clear, cornerRadius: 0)
        
        hotPickLabel = Utilities.sharedInstance.createLabelWith(text: "HOT PICK", txtAlignment: .left, font: .boldSystemFont(ofSize: 12), textColor: .white, backgroundColor: .clear)
        hotPickLabel.numberOfLines = 1
        
        self.addSubview(hotPickImageView)
        self.addSubview(hotPickLabel)
    }
    
    //MARK:- Setting Up Constraints
    func setupConstraints() {
        hotPickImageView.snp.makeConstraints { (make) in
            make.left.equalTo(self).offset(5)
            make.height.width.equalTo(self.snp.height).dividedBy(1.1)
        }
        
        hotPickLabel.snp.makeConstraints { (make) in
            make.left.equalTo(hotPickImageView.snp.right).offset(5)
            make.right.equalTo(self).offset(-5)
            make.centerY.equalTo(self)
        }
    }
    
    //MARK:- Setting Gradient Background
    func setGradientBackground() {
        let gradientLayer: CAGradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
        
        let topColor: CGColor = UIColor(red: 0.90, green: 0.44, blue: 0.22, alpha: 1.00).cgColor
        let bottomColor: CGColor = UIColor(red: 0.84, green: 0.34, blue: 0.27, alpha: 1.00).cgColor
        
        gradientLayer.colors = [topColor, bottomColor]
        
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
}
