//
//  DiscoverViewController.swift
//  AjmoAppDemo
//
//  Created by Nenad Ljubik on 10/7/20.
//  Copyright © 2020 Nenad Ljubik. All rights reserved.
//

import UIKit
import SnapKit

class DiscoverViewController: UIViewController {
    
    //UI
    var tableView: UITableView!
    
    //Helpers
    var customPicks = [CustomPick]()
    var bestCoffeeVenues = [Venue]()
    var liveGigsVenues = [Venue]()
    var trendingVenues = [Venue]()
    
    //MARK:- Controller Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Discover"
        
        setupViews()
        setupConstraints()
        getVenues()
    }
    
    //MARK:- Setting Up Views
    func setupViews() {
        tableView = UITableView()
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .groupTableViewBackground
        tableView.showsVerticalScrollIndicator = false
        tableView.register(DiscoverTableViewCell.self, forCellReuseIdentifier: "cell")
        
        tableView.delegate = self
        tableView.dataSource = self
        
        self.view.addSubview(tableView)
    }
    
    //MARK:- Setting Up Constraints
    func setupConstraints() {
        tableView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
    }
    
    //MARK:- Creating View For Header Section
    func createViewForHeaderSection(headerTitle: String) -> UIView {
        let headerView = UIView()
        headerView.backgroundColor = .groupTableViewBackground
        
        let titleHeaderLabel = Utilities.sharedInstance.createLabelWith(text: headerTitle, txtAlignment: .left, font: .boldSystemFont(ofSize: 20), textColor: .black, backgroundColor: .clear)
        
        headerView.addSubview(titleHeaderLabel)
        
        titleHeaderLabel.snp.makeConstraints { (make) in
            make.top.bottom.right.equalTo(headerView)
            make.left.equalTo(headerView).offset(20)
        }
        
        return headerView
    }
}

//MARK:- UITableView Delegate And DataSource Methods
extension DiscoverViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return customPicks.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DiscoverTableViewCell
        cell.setupCell(customPick: customPicks[indexPath.section])
        cell.delegate = self
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.size.width / 1.2
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionTitle = customPicks[section].title ?? ""
        let headerView = createViewForHeaderSection(headerTitle: sectionTitle)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
}

//MARK:- DiscoverTableViewCell Delegate Methods
extension DiscoverViewController: DiscoverTableViewCellDelegate {
    func tagWasTapped(tagName: String) {
        var venuesGroupedByTag = [Venue]()
        venuesGroupedByTag = Utilities.sharedInstance.getSortedAndFilteredVenues(tagName: tagName)
        
        navigationController?.pushViewController(VenuesListViewController(venues: venuesGroupedByTag, tagName: tagName), animated: true)
    }
    
    func venueWasTapped(venue: Venue) {
        navigationController?.pushViewController(VenueDetailsViewController(venue: venue), animated: true)
    }
}


//MARK:- API Communication
extension DiscoverViewController {
    //Get Venues API Request
    func getVenues() {
        if (Utilities.sharedInstance.checkForInternetConnection()) {
            Utilities.sharedInstance.shouldShowIndicator(view: self.view, shouldAnimate: true)
            ApiManager.sharedInstance.getVenues { (success, responseData, statusCode) in
                if (success) {
                    if let data = responseData?["data"] as? [String:Any] {
                        let jsonDecoder = JSONDecoder()
                        
                        if let customPicks = data["customPicks"] as? [[String:Any]] {
                            for customPick in customPicks {
                                if let customPickData = Utilities.sharedInstance.jsonToData(json: customPick) {
                                    do {
                                        let customP = try jsonDecoder.decode(CustomPick.self, from: customPickData)
                                        if let customPickTitle = customP.title, let venues = customP.items {
                                            if (customPickTitle == "Best coffee") {
                                                self.bestCoffeeVenues = venues
                                                Utilities.sharedInstance.coffeeVenues = self.bestCoffeeVenues
                                            } else if (customPickTitle == "Live gigs") {
                                                self.liveGigsVenues = venues
                                                Utilities.sharedInstance.liveGigsVenues = self.liveGigsVenues
                                            }
                                        }
                                        self.customPicks.append(customP)
                                    } catch {
                                        print(error.localizedDescription)
                                    }
                                }
                            }
                            self.tableView.reloadData()
                        }
                        
                        if let trendingVenuesJSON = data["trending"] as? [[String:Any]] {
                            for trendingVenueJSON in trendingVenuesJSON {
                                if let trendingVenueData = Utilities.sharedInstance.jsonToData(json: trendingVenueJSON) {
                                    do {
                                        let trendingVenue = try jsonDecoder.decode(Venue.self, from: trendingVenueData)
                                        self.trendingVenues.append(trendingVenue)
                                        Utilities.sharedInstance.trendingVenues = self.trendingVenues
                                    } catch {
                                        print(error.localizedDescription)
                                    }
                                }
                            }
                        }
                    } else {
                        Utilities.sharedInstance.createAlert(title: "There Is Some Kind Of Problem. Please Try Again", message: "", controller: self)
                    }
                } else {
                    Utilities.sharedInstance.createAlert(title: "There Is Some Kind Of Problem. Please Try Again", message: "", controller: self)
                }
                Utilities.sharedInstance.shouldShowIndicator(view: self.view, shouldAnimate: false)
            }
        } else {
            Utilities.sharedInstance.createAlertForNoInternetConnection(controller: self)
        }
    }
}
