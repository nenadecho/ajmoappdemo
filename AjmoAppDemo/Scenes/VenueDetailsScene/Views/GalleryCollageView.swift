//
//  GalleryCollageView.swift
//  AjmoAppDemo
//
//  Created by Nenad Ljubik on 10/8/20.
//  Copyright © 2020 Nenad Ljubik. All rights reserved.
//

import UIKit

enum CollageType {
    case OneImage
    case TwoImages
    case ThreeAndMoreImages
    
}

protocol GalleryCollageViewDelegate {
    func tappedImageViewWith(tag: Int)
}

class GalleryCollageView: UIView {
    
    //UI
    var firstImageView: UIImageView!
    var secondImageView: UIImageView!
    var thirdImageView: UIImageView!
    
    var helperView: UIView!
    var galleryCountLabel: UILabel!
    
    //Helpers
    var collageType: CollageType!
    var delegate: GalleryCollageViewDelegate!
    
    //MARK:- View Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- Setting Up Views
    func setupViews() {
        firstImageView = Utilities.sharedInstance.createImageViewWith(imageName: "", contentMode: .scaleAspectFill, backgroundColor: .clear, cornerRadius: 0)
        firstImageView.tag = 0
        firstImageView.isUserInteractionEnabled = true
        firstImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(imageViewTapped(sender:))))
        firstImageView.layer.masksToBounds = true
        
        secondImageView = Utilities.sharedInstance.createImageViewWith(imageName: "", contentMode: .scaleAspectFill, backgroundColor: .clear, cornerRadius: 0)
        secondImageView.isUserInteractionEnabled = true
        secondImageView.tag = 1
        secondImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(imageViewTapped(sender:))))
        secondImageView.layer.masksToBounds = true
        
        thirdImageView = Utilities.sharedInstance.createImageViewWith(imageName: "", contentMode: .scaleAspectFill, backgroundColor: .clear, cornerRadius: 0)
        thirdImageView.isUserInteractionEnabled = true
        thirdImageView.tag = 2
        thirdImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(imageViewTapped(sender:))))
        thirdImageView.layer.masksToBounds = true
        
        galleryCountLabel = Utilities.sharedInstance.createLabelWith(text: "", txtAlignment: .center, font: .systemFont(ofSize: 14), textColor: .white, backgroundColor: UIColor.black.withAlphaComponent(0.5))
        galleryCountLabel.layer.borderColor = UIColor.white.cgColor
        galleryCountLabel.layer.borderWidth = 1
        
        self.addSubview(firstImageView)
        self.addSubview(secondImageView)
        self.addSubview(thirdImageView)
        self.addSubview(galleryCountLabel)
        
    }
    
    //MARK:- Setting Up Constraints
    func setupConstraints() {
        galleryCountLabel.snp.makeConstraints { (make) in
            make.width.height.equalTo(30)
            make.left.equalTo(self).offset(20)
            make.bottom.equalTo(self).offset(-20)
        }
        
        switch collageType {
        case .OneImage:
            setupConstraintsForOneImage()
        case .TwoImages:
            setupConstraintsForTwoImages()
        case .ThreeAndMoreImages:
            setupConstraintsForThreeOrMoreImages()
        default:
            return
        }
    }
    
    //MARK:- Setting Up Constraints For One Image
    func setupConstraintsForOneImage() {
        firstImageView.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
    }
    
    //MARK:- Setting Up Constraints For Two Images
    func setupConstraintsForTwoImages() {
        firstImageView.snp.makeConstraints { (make) in
            make.left.top.bottom.equalTo(self)
            make.right.equalTo(self.snp.centerX).offset(-2.5)
        }
        
        secondImageView.snp.makeConstraints { (make) in
            make.left.equalTo(self.snp.centerX).offset(2.5)
            make.right.top.bottom.equalTo(self)
        }
    }
    
    //MARK:- Setting Up Constraints For Three Or More Images
    func setupConstraintsForThreeOrMoreImages() {
        firstImageView.snp.makeConstraints { (make) in
            make.left.top.bottom.equalTo(self)
            make.width.equalTo(self).dividedBy(1.5)
        }
        
        secondImageView.snp.makeConstraints { (make) in
            make.left.equalTo(firstImageView.snp.right).offset(5)
            make.top.equalTo(self)
            make.bottom.equalTo(firstImageView.snp.centerY).offset(-2.5)
        }
        
        thirdImageView.snp.makeConstraints { (make) in
            make.left.equalTo(firstImageView.snp.right).offset(5)
            make.bottom.equalTo(self)
            make.top.equalTo(firstImageView.snp.centerY).offset(2.5)
        }
    }
    
    //MARK:- Set Gallery Images
    func setGalleryImages(gallery: [Gallery]) {
        if (gallery.count == 1) {
            self.collageType = .OneImage
            if let firstImage = gallery.first, let imageName = firstImage.picture {
                firstImageView.kf.setImage(with: URL(string: imageName))
            }
        } else if (gallery.count == 2) {
            self.collageType = .TwoImages
            if let firstImage = gallery.first, let imageName = firstImage.picture {
                firstImageView.kf.setImage(with: URL(string: imageName))
            }
            
            if let imageName = gallery[1].picture {
                secondImageView.kf.setImage(with: URL(string: imageName))
            }
            
        } else if (gallery.count > 2) {
            self.collageType = .ThreeAndMoreImages
            if let firstImage = gallery.first, let imageName = firstImage.picture {
                firstImageView.kf.setImage(with: URL(string: imageName))
            }
            
            if let imageName = gallery[1].picture {
                secondImageView.kf.setImage(with: URL(string: imageName))
            }
            
            if let imageName = gallery[2].picture {
                thirdImageView.kf.setImage(with: URL(string: imageName))
            }
        }
        
        setupConstraints()
        
        galleryCountLabel.text = "\(gallery.count)"
    }
    
    //MARK:- TapGesture Action
    @objc func imageViewTapped(sender: UITapGestureRecognizer) {
        if let tag = sender.view?.tag {
            delegate.tappedImageViewWith(tag: tag)
        }
    }
}
