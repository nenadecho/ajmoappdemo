//
//  WorkHoursView.swift
//  AjmoAppDemo
//
//  Created by Nenad Ljubik on 10/8/20.
//  Copyright © 2020 Nenad Ljubik. All rights reserved.
//

import UIKit

protocol WorkHoursViewDelegate {
    func okButtonTapped()
}

class WorkHoursView: UIView {

    //UI
    var titleLabel: UILabel!
    var openedLabel: UILabel!
    var stackView: UIStackView!
    var button: UIButton!
    
    //Helpers
    var workingHours: [WorkingHour]!
    var delegate: WorkHoursViewDelegate!
    
    //MARK:- View Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- Setting Up Views
    func setupViews() {
        self.backgroundColor = .white
        self.layer.cornerRadius = 10
        
        titleLabel = Utilities.sharedInstance.createLabelWith(text: "Hours", txtAlignment: .center, font: .boldSystemFont(ofSize: 18), textColor: .black, backgroundColor: .clear)
        
        openedLabel = Utilities.sharedInstance.createLabelWith(text: "", txtAlignment: .center, font: .boldSystemFont(ofSize: 16), textColor: .black, backgroundColor: .clear)
        
        button = Utilities.sharedInstance.createButtonWith(title: "OK", titleColor: .black, font: .boldSystemFont(ofSize: 16), corner: 0, backgroundColor: .clear, tag: 0)
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        
        stackView = UIStackView()
        stackView.axis = .vertical
        stackView.backgroundColor = .yellow
        stackView.distribution = .fillEqually
        stackView.spacing = 5

        self.addSubview(titleLabel)
        self.addSubview(openedLabel)
        self.addSubview(stackView)
        self.addSubview(button)
    }
    
    //MARK:- Setting Up Constraints
    func setupConstraints() {
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self).offset(10)
            make.centerX.equalTo(self)
        }
        
        openedLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(10)
            make.centerX.equalTo(self)
        }
        
        stackView.snp.makeConstraints { (make) in
            make.left.equalTo(self).offset(20)
            make.right.equalTo(self).offset(-20)
            make.top.equalTo(openedLabel.snp.bottom).offset(10)
            make.bottom.equalTo(button.snp.top).offset(-5)
        }
        
        button.snp.makeConstraints { (make) in
            make.bottom.equalTo(self).offset(-5)
            make.centerX.equalTo(self)
            make.left.right.equalTo(self)
        }
    }
    
    @objc func buttonTapped() {
        delegate.okButtonTapped()
    }
    
    //MARK:- Setup StackView Data
    func setupStackViewData(venue: Venue) {
        self.workingHours = venue.working_hours
        
        if let isOpened = venue.opened {
            if (isOpened) {
                openedLabel.text = "Opened"
                openedLabel.textColor = .systemGreen
            } else {
                openedLabel.text = "Closed"
                openedLabel.textColor = .systemRed
            }
        }
        
        let mondayLabel = Utilities.sharedInstance.createLabelWith(text: "MON \(getWorkingHoursFor(dayOfTheWeek: 0))", txtAlignment: .center, font: .boldSystemFont(ofSize: 16), textColor: .lightGray, backgroundColor: .clear)
        let tuesdayLabel = Utilities.sharedInstance.createLabelWith(text: "TUE \(getWorkingHoursFor(dayOfTheWeek: 1))", txtAlignment: .center, font: .boldSystemFont(ofSize: 16), textColor: .lightGray, backgroundColor: .clear)
        let wednesdayLabel = Utilities.sharedInstance.createLabelWith(text: "WED \(getWorkingHoursFor(dayOfTheWeek: 2))", txtAlignment: .center, font: .boldSystemFont(ofSize: 16), textColor: .lightGray, backgroundColor: .clear)
        let thursdayLabel = Utilities.sharedInstance.createLabelWith(text: "THU \(getWorkingHoursFor(dayOfTheWeek: 3))", txtAlignment: .center, font: .boldSystemFont(ofSize: 16), textColor: .lightGray, backgroundColor: .clear)
        let fridayLabel = Utilities.sharedInstance.createLabelWith(text: "FRI \(getWorkingHoursFor(dayOfTheWeek: 4))", txtAlignment: .center, font: .boldSystemFont(ofSize: 16), textColor: .lightGray, backgroundColor: .clear)
        let saturdayLabel = Utilities.sharedInstance.createLabelWith(text: "SAT \(getWorkingHoursFor(dayOfTheWeek: 5))", txtAlignment: .center, font: .boldSystemFont(ofSize: 16), textColor: .lightGray, backgroundColor: .clear)
        let sundayLabel = Utilities.sharedInstance.createLabelWith(text: "SUN \(getWorkingHoursFor(dayOfTheWeek: 6))", txtAlignment: .center, font: .boldSystemFont(ofSize: 16), textColor: .lightGray, backgroundColor: .clear)
        
        stackView.addArrangedSubview(mondayLabel)
        stackView.addArrangedSubview(tuesdayLabel)
        stackView.addArrangedSubview(wednesdayLabel)
        stackView.addArrangedSubview(thursdayLabel)
        stackView.addArrangedSubview(fridayLabel)
        stackView.addArrangedSubview(saturdayLabel)
        stackView.addArrangedSubview(sundayLabel)
    }
    
    //MARK:- Get The Working Hour For Specific Day
    func getWorkingHoursFor(dayOfTheWeek: Int) -> String {
        let workingHour = workingHours?.first(where: { (workingHour) -> Bool in
            if let day = workingHour.day {
                if (day == dayOfTheWeek) {
                    return true
                } else {
                    return false
                }
            } else {
                return false
            }
        })
        
        if let start = workingHour?.start, let end = workingHour?.end {
           return "\(start) - \(end)"
        } else {
            return ""
        }
    }
}
