//
//  VenueGeneralInfoTableViewCell.swift
//  AjmoAppDemo
//
//  Created by Nenad Ljubik on 10/8/20.
//  Copyright © 2020 Nenad Ljubik. All rights reserved.
//

import UIKit

protocol VenueGeneralInfoTableViewCellDelegate {
    func workHourTapped()
    func locationTapped()
}

class VenueGeneralInfoTableViewCell: UITableViewCell {
    
    //UI
    var venueNameLabel: UILabel!
    var venueCategoryLabel: UILabel!
    var workingTimeImageView: UIImageView!
    var workingTimeLabel: UILabel!
    var openedLabel: UILabel!
    var locationImageView: UIImageView!
    var venueAddressLabel: UILabel!
    
    //Helpers
    var venue: Venue!
    var delegate: VenueGeneralInfoTableViewCellDelegate!
    
    //MARK:- Cell Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- Setting Up Views
    func setupViews() {
        self.selectionStyle = .none
        self.backgroundColor = .clear
        
        venueNameLabel = Utilities.sharedInstance.createLabelWith(text: "", txtAlignment: .left, font: .systemFont(ofSize: 22), textColor: .black, backgroundColor: .clear)
        
        venueCategoryLabel = Utilities.sharedInstance.createLabelWith(text: "", txtAlignment: .left, font: .systemFont(ofSize: 14), textColor: .gray, backgroundColor: .clear)
        
        workingTimeImageView = Utilities.sharedInstance.createImageViewWith(imageName: "clockIcon", contentMode: .scaleAspectFit, backgroundColor: .clear, cornerRadius: 0)
        workingTimeImageView.isUserInteractionEnabled = true
        workingTimeImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tappedWorkHours)))
        
        workingTimeLabel = Utilities.sharedInstance.createLabelWith(text: "", txtAlignment: .left, font: .systemFont(ofSize: 16), textColor: .black, backgroundColor: .clear)
        workingTimeLabel.isUserInteractionEnabled = true
        workingTimeLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tappedWorkHours)))
        
        locationImageView = Utilities.sharedInstance.createImageViewWith(imageName: "navigationIcon", contentMode: .scaleAspectFit, backgroundColor: .clear, cornerRadius: 0)
        locationImageView.isUserInteractionEnabled = true
        locationImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tappedAddress)))
        
        openedLabel = Utilities.sharedInstance.createLabelWith(text: "", txtAlignment: .left, font: .boldSystemFont(ofSize: 14), textColor: .systemGreen, backgroundColor: .clear)
        openedLabel.isUserInteractionEnabled = true
        openedLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tappedWorkHours)))
        
        venueAddressLabel = Utilities.sharedInstance.createLabelWith(text: "", txtAlignment: .left, font: .systemFont(ofSize: 16), textColor: .black, backgroundColor: .clear)
        venueAddressLabel.isUserInteractionEnabled = true
        venueAddressLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tappedAddress)))
        
        
        self.contentView.addSubview(venueNameLabel)
        self.contentView.addSubview(venueCategoryLabel)
        self.contentView.addSubview(workingTimeImageView)
        self.contentView.addSubview(workingTimeLabel)
        self.contentView.addSubview(openedLabel)
        self.contentView.addSubview(locationImageView)
        self.contentView.addSubview(venueAddressLabel)
    }
    
    //MARK:- Setting Up Constraints
    func setupConstraints() {
        venueNameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.contentView).offset(10)
            make.right.equalTo(self.contentView).offset(-20)
            make.left.equalTo(self.contentView).offset(20)
        }
        
        venueCategoryLabel.snp.makeConstraints { (make) in
            make.top.equalTo(venueNameLabel.snp.bottom).offset(10)
            make.left.equalTo(self.contentView).offset(15)
            make.right.equalTo(venueNameLabel)
        }
        
        workingTimeImageView.snp.makeConstraints { (make) in
            make.left.equalTo(venueCategoryLabel)
            make.top.equalTo(venueCategoryLabel.snp.bottom).offset(20)
            make.height.width.equalTo(25)
        }
        
        workingTimeLabel.snp.makeConstraints { (make) in
            make.left.equalTo(workingTimeImageView.snp.right).offset(10)
            make.right.equalTo(self.snp.centerX).offset(10)
            make.centerY.equalTo(workingTimeImageView)
        }
        
        openedLabel.snp.makeConstraints { (make) in
            make.left.equalTo(workingTimeLabel.snp.right).offset(10)
            make.right.equalTo(self.contentView).offset(-20)
            make.centerY.equalTo(workingTimeImageView)
        }
        
        locationImageView.snp.makeConstraints { (make) in
            make.top.equalTo(workingTimeImageView.snp.bottom).offset(20)
            make.left.equalTo(venueCategoryLabel)
            make.height.width.equalTo(25)
            make.bottom.equalTo(self.contentView).offset(-20)
        }
        
        venueAddressLabel.snp.makeConstraints { (make) in
            make.left.equalTo(locationImageView.snp.right).offset(10)
            make.centerY.equalTo(locationImageView)
            make.right.equalTo(self.contentView).offset(-20)
        }
    }
    
    //MARK:- Setup Cell
    func setupCell(venue: Venue) {
        self.venue = venue
        
        venueNameLabel.text = venue.name ?? ""
        
        venueCategoryLabel.text = venue.subtitle ?? ""
        
        handleCurrentDayName()
        
        venueAddressLabel.text = venue.address ?? ""
        
        if let isOpened = venue.opened {
            if (isOpened) {
                openedLabel.text = "Opened"
                openedLabel.textColor = .systemGreen
            } else {
                openedLabel.text = "Closed"
                openedLabel.textColor = .systemRed
            }
        }
    }
    
    //MARK:- TapGesture Action For Work Hours
    @objc func tappedWorkHours() {
        delegate.workHourTapped()
    }
    
    //MARK:- TapGesture Action For Adress
    @objc func tappedAddress() {
        delegate.locationTapped()
    }
    
    //MARK:- Handle Current Day Name
    func handleCurrentDayName() {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        let dayInWeek = dateFormatter.string(from: date)
        
        switch dayInWeek {
        case "Monday":
            getWorkingHoursFor(dayOfTheWeek: 0)
        case "Tuesday":
            getWorkingHoursFor(dayOfTheWeek: 1)
        case "Wednesday":
            getWorkingHoursFor(dayOfTheWeek: 2)
        case "Thursday":
            getWorkingHoursFor(dayOfTheWeek: 3)
        case "Friday":
            getWorkingHoursFor(dayOfTheWeek: 4)
        case "Saturday":
            getWorkingHoursFor(dayOfTheWeek: 5)
        case "Sunday":
            getWorkingHoursFor(dayOfTheWeek: 6)
        default:
            break
        }
    }
    
    //MARK:- Get The Working Hour For Specific Day
    func getWorkingHoursFor(dayOfTheWeek: Int) {
        let workingHour = venue.working_hours?.first(where: { (workingHour) -> Bool in
            if let day = workingHour.day {
                if (day == dayOfTheWeek) {
                    return true
                } else {
                    return false
                }
            } else {
                return false
            }
        })
        
        if let start = workingHour?.start, let end = workingHour?.end {
            workingTimeLabel.text = "\(start) - \(end)"
        }
    }
}
