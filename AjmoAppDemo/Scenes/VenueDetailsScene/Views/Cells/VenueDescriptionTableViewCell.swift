//
//  VenueDescriptionTableViewCell.swift
//  AjmoAppDemo
//
//  Created by Nenad Ljubik on 10/8/20.
//  Copyright © 2020 Nenad Ljubik. All rights reserved.
//

import UIKit

class VenueDescriptionTableViewCell: UITableViewCell {
    
    //UI
    var venueIdealPlaceLabel: UILabel!
    var venueDescriptionLabel: UILabel!
    
    //MARK:- Cell Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- Setting Up Views
    func setupViews() {
        self.selectionStyle = .none
        self.backgroundColor = .clear
        
        self.venueIdealPlaceLabel = Utilities.sharedInstance.createLabelWith(text: "Ideal place for...", txtAlignment: .left, font: .boldSystemFont(ofSize: 16), textColor: .gray, backgroundColor: .clear)
        
        venueDescriptionLabel = Utilities.sharedInstance.createLabelWith(text: "", txtAlignment: .left, font: .boldSystemFont(ofSize: 18), textColor: .black, backgroundColor: .clear)
        venueDescriptionLabel.numberOfLines = 0
        venueDescriptionLabel.lineBreakMode = .byWordWrapping
        
        self.contentView.addSubview(venueIdealPlaceLabel)
        self.contentView.addSubview(venueDescriptionLabel)
    }
    
    //MARK:- Setting Up Constraints
    func setupConstraints() {
        venueIdealPlaceLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.contentView).offset(20)
            make.top.equalTo(self.contentView).offset(30)
        }
        
        venueDescriptionLabel.snp.makeConstraints { (make) in
            make.top.equalTo(venueIdealPlaceLabel.snp.bottom).offset(20)
            make.left.equalTo(venueIdealPlaceLabel)
            make.right.equalTo(self.contentView).offset(-20)
            make.bottom.equalTo(self.contentView).offset(-20)
        }
    }
    
    //MARK:- Setup Cell
    func setupCell(venue: Venue) {
        if let venueDescription = venue.description {
            venueDescriptionLabel.attributedText = venueDescription.htmlToAttributedString
        }
    }
}

extension String {
    var htmlToAttributedString: NSMutableAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            let convertedString = try NSMutableAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
            let textRangeForFont : NSRange = NSMakeRange(0, convertedString.length)
            convertedString.addAttributes([NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16)], range: textRangeForFont)
            return convertedString
        } catch {
            return nil
        }
    }
    
    
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
