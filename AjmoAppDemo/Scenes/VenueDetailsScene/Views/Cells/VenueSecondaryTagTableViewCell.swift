//
//  VenueSecondaryTagTableViewCell.swift
//  AjmoAppDemo
//
//  Created by Nenad Ljubik on 10/8/20.
//  Copyright © 2020 Nenad Ljubik. All rights reserved.
//

import UIKit

protocol VenueSecondaryTagTableViewCellDelegate {
    func secondaryTagTappedWith(tagName: String)
}

class VenueSecondaryTagTableViewCell: UITableViewCell {
    
    //UI
    var titleLabel: UILabel!
    var tagsCollectionView: UICollectionView!
    
    //Helpers
    var tags = [Tag]()
    var delegate: VenueSecondaryTagTableViewCellDelegate!
    
    //MARK:- Cell Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        tagsCollectionView.snp.remakeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(15)
            make.height.equalTo(tagsCollectionView.contentSize.height + 10)
            make.left.right.equalTo(self)
            make.bottom.equalTo(self)
        }
    }
    
    //MARK:- Setting Up Views
    func setupViews() {
        self.selectionStyle = .none
        self.backgroundColor = .clear
        
        titleLabel = Utilities.sharedInstance.createLabelWith(text: "What to expect", txtAlignment: .left, font: .boldSystemFont(ofSize: 18), textColor: .black, backgroundColor: .clear)
        
          let layout = TagCollectionViewFlowLayout()
        layout.estimatedItemSize = CGSize(width: 140, height: 40)
        layout.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        
        
        tagsCollectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: 0), collectionViewLayout: layout)
        tagsCollectionView.layoutIfNeeded()
        tagsCollectionView.showsVerticalScrollIndicator = false
        tagsCollectionView.showsHorizontalScrollIndicator = false
        tagsCollectionView.backgroundColor = .clear
        tagsCollectionView.register(TagCollectionViewCell.self, forCellWithReuseIdentifier: "tagCell")
        tagsCollectionView.delegate = self
        tagsCollectionView.dataSource = self
        
        self.addSubview(titleLabel)
        self.addSubview(tagsCollectionView)
    }
    
    //MARK:- Setting Up Constraints
    func setupConstraints() {
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self).offset(10)
            make.left.equalTo(self).offset(20)
            make.right.equalTo(self).offset(-20)
        }
        
        tagsCollectionView.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(15)
            make.height.equalTo(tagsCollectionView.contentSize.height)
            make.left.right.equalTo(self)
            make.bottom.equalTo(self).offset(-10)
        }
    }
    
    //MARK:- Setup Cell
    func setupCell(venue: Venue) {
        if let tags = venue.allTags?.secondaryTags {
            self.tags = tags
            tagsCollectionView.reloadData()
        }
    }
}

//MARK:- UICollectionView Delegate, DataSource And DelegateFlowLayout Methods
extension VenueSecondaryTagTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tags.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tagCell", for: indexPath) as! TagCollectionViewCell
        cell.setupCellForSecondaryTag(tag: tags[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let tagName = tags[indexPath.row].name {
            delegate.secondaryTagTappedWith(tagName: tagName)
        }
    }
}
