//
//  VenueGalleryTableViewCell.swift
//  AjmoAppDemo
//
//  Created by Nenad Ljubik on 10/8/20.
//  Copyright © 2020 Nenad Ljubik. All rights reserved.
//

import UIKit

protocol VenueGalleryTableViewCellDelegate {
    func galleryImageTappedWith(index: Int)
}

class VenueGalleryTableViewCell: UITableViewCell {
    
    //UI
    var galleryCollageView: GalleryCollageView!
    
    //Helpers
    var delegate: VenueGalleryTableViewCellDelegate!
    
    //MARK:- Cell Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- Setting Up Views
    func setupViews() {
        self.selectionStyle = .none
        self.backgroundColor = .clear
        
        galleryCollageView = GalleryCollageView()
        galleryCollageView.delegate = self
        
        self.addSubview(galleryCollageView)
    }
    
    //MARK:- Setting Up Constraints
    func setupConstraints() {
        galleryCollageView.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
    }
    
    //MARK:- Setup Cell
    func setupCell(venue: Venue) {
        if let venueGallery = venue.gallery {
            galleryCollageView.setGalleryImages(gallery: venueGallery)
        }
    }
}

//MARK:- GalleryCollageView Delegate Methods
extension VenueGalleryTableViewCell: GalleryCollageViewDelegate {
    func tappedImageViewWith(tag: Int) {
        delegate.galleryImageTappedWith(index: tag)
    }
}
