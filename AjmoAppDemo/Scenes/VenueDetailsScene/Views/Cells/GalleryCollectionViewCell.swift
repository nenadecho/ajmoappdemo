//
//  GalleryCollectionViewCell.swift
//  AjmoAppDemo
//
//  Created by Nenad Ljubik on 10/8/20.
//  Copyright © 2020 Nenad Ljubik. All rights reserved.
//

import UIKit

class GalleryCollectionViewCell: UICollectionViewCell {
    
    //UI
    var imageView: UIImageView!
    
    //MARK:- Cell Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- Setting Up Views
    func setupViews() {
        self.backgroundColor = .clear
        
        imageView = Utilities.sharedInstance.createImageViewWith(imageName: "", contentMode: .scaleAspectFit, backgroundColor: .clear, cornerRadius: 0)
        imageView.layer.masksToBounds = true
        
        self.addSubview(imageView)
    }
    
    //MARK:- Setting Up Constraints
    func setupConstraints() {
        imageView.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
    }
    
    //MARK:- Setup Cell
    func setupCell(gallery: Gallery) {
        if let imageName = gallery.picture {
            imageView.kf.setImage(with: URL(string: imageName))
        }
    }
}
