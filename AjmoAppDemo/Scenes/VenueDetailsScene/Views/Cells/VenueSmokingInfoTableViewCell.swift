//
//  VenueSmokingInfoTableViewCell.swift
//  AjmoAppDemo
//
//  Created by Nenad Ljubik on 10/8/20.
//  Copyright © 2020 Nenad Ljubik. All rights reserved.
//

import UIKit

class VenueSmokingInfoTableViewCell: UITableViewCell {
    
    //UI
    var smokingInfoImageView: UIImageView!
    var smokingInfoLabel: UILabel!
    
    //MARK:- Cell Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- Setting Up Views
    func setupViews() {
        self.selectionStyle = .none
        self.backgroundColor = .clear
        
        smokingInfoImageView = Utilities.sharedInstance.createImageViewWith(imageName: "", contentMode: .scaleAspectFit, backgroundColor: Utilities.sharedInstance.colorFromHexCode(hex: "FFC75F"), cornerRadius: self.contentView.frame.size.height / 1.1 / 2)
        
        smokingInfoLabel = Utilities.sharedInstance.createLabelWith(text: "", txtAlignment: .left, font: .boldSystemFont(ofSize: 14), textColor: .gray, backgroundColor: .clear)
        
        self.contentView.addSubview(smokingInfoImageView)
        self.contentView.addSubview(smokingInfoLabel)
    }
    
    //MARK:- Setting Up Constraints
    func setupConstraints() {
        smokingInfoImageView.snp.makeConstraints { (make) in
            make.left.equalTo(20)
            make.centerY.equalTo(self.contentView)
            make.width.height.equalTo(self.contentView.frame.size.height/1.1)
        }
        
        smokingInfoLabel.snp.makeConstraints { (make) in
            make.left.equalTo(smokingInfoImageView.snp.right).offset(10)
            make.centerY.equalTo(self.contentView)
            make.right.equalTo(self.contentView).offset(-20)
        }
    }
    
    //MARK:- Setup Cell
    func setupCell(venue: Venue) {
        if let hasSmokingArea = venue.smoking_area {
            hasSmokingArea == 1 ? (smokingInfoLabel.text = "Smoking area") : (smokingInfoLabel.text = "No smoking area")
            hasSmokingArea == 1 ? (smokingInfoImageView.image = UIImage(named: "smokingIcon")) : (smokingInfoImageView.image = UIImage(named: "noSmokingIcon"))
        } else {
            smokingInfoLabel.text = "No smoking area"
            smokingInfoImageView.image = UIImage(named: "noSmokingIcon")
        }
    }
}
