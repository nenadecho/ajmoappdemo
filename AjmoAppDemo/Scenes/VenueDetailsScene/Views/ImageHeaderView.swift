//
//  ImageHeaderView.swift
//  AjmoAppDemo
//
//  Created by Nenad Ljubik on 10/8/20.
//  Copyright © 2020 Nenad Ljubik. All rights reserved.
//

import UIKit

class ImageHeaderView: UIView {
    
    //UI
    var imageView: UIImageView!
    var hotPickView: HotPickView!
    
    //MARK:- View Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- Setting Up Views
    func setupViews() {
        imageView = Utilities.sharedInstance.createImageViewWith(imageName: "", contentMode: .scaleAspectFill, backgroundColor: .clear, cornerRadius: 0)
        imageView.layer.masksToBounds = true
        
        hotPickView = HotPickView()
        
        self.addSubview(hotPickView)
        self.addSubview(imageView)
        
        self.bringSubviewToFront(hotPickView)
    }
    
    //MARK:- Setting Up Constraints
    func setupConstraints() {
        imageView.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
        
        hotPickView.snp.makeConstraints { (make) in
            make.right.equalTo(self).offset(-20)
            make.bottom.equalTo(self).offset(-20)
            make.height.equalTo(35)
            make.width.equalTo(110)
        }
    }
}
