//
//  GalleryViewController.swift
//  AjmoAppDemo
//
//  Created by Nenad Ljubik on 10/8/20.
//  Copyright © 2020 Nenad Ljubik. All rights reserved.
//

import UIKit

class GalleryViewController: UIViewController {

    //UI
    var collectionView: UICollectionView!
    
    //Helpers
    var galleries = [Gallery]()
    var selectedIndex = 0
    
    init(galleries: [Gallery], selectedIndex: Int) {
        super.init(nibName: nil, bundle: nil)
        
        self.galleries = galleries
        self.selectedIndex = selectedIndex
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- Controller Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
        setupConstraints()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setImageOnNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.collectionView.scrollToItem(at: IndexPath(row: selectedIndex, section: 0), at: .centeredHorizontally, animated: false)
        collectionView.isHidden = false
    }
    
    //MARK:- Setting Up Views
    func setupViews() {
        self.view.backgroundColor = .black
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(barButtonAction))
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.isHidden = true
        collectionView.isPagingEnabled = true
        collectionView.register(GalleryCollectionViewCell.self, forCellWithReuseIdentifier: "galleryCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        
        
        
        self.view.addSubview(collectionView)
    }
    
    //MARK:- Setting Up Constraints
    func setupConstraints() {
        collectionView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
    }
    
    
    //MARK:- DoneBarButton Item Action
    @objc func barButtonAction() {
        dismiss(animated: true, completion: nil)
    }
    
    func setImageOnNavigationBar() {
        if let navigationBar = self.navigationController?.navigationBar {
            navigationBar.tintColor = .white
            let gradient = CAGradientLayer()
            var bounds = navigationBar.bounds
            bounds.size.height += UIApplication.shared.statusBarFrame.size.height
            gradient.frame = bounds
            gradient.colors = [Utilities.sharedInstance.colorFromHexCode(hex: "FFC75F").cgColor, Utilities.sharedInstance.colorFromHexCode(hex: "FF9671").cgColor]
            gradient.startPoint = CGPoint(x: 0, y: 0)
            gradient.endPoint = CGPoint(x: 1, y: 0)
            
            if let image = Utilities.sharedInstance.getImageFrom(gradientLayer: gradient) {
                navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
            }
        }
    }
}

//MARK:- UICollectionView Delegate, DataSource And DelegateFlowLayout Methods
extension GalleryViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return galleries.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "galleryCell", for: indexPath) as! GalleryCollectionViewCell
        cell.setupCell(gallery: galleries[indexPath.row])
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height)
    }
}
