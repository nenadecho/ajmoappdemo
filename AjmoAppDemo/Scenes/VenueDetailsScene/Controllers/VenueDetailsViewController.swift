//
//  VenueDetailsViewController.swift
//  AjmoAppDemo
//
//  Created by Nenad Ljubik on 10/8/20.
//  Copyright © 2020 Nenad Ljubik. All rights reserved.
//

import UIKit
import MapKit

class VenueDetailsViewController: UIViewController {
    
    //UI
    var tableHeaderView: ImageHeaderView!
    var tableView: UITableView!
    var workHoursView: WorkHoursView!
    
    //Helpers
    var venue: Venue!
    
    //MARK:- Controller Initialization
    init(venue: Venue) {
        super.init(nibName: nil, bundle: nil)
        self.venue = venue
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- Controller Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        setupConstraints()
    }
    
    //MARK:- Setting Up Views
    func setupViews() {
        
        
        tableHeaderView = ImageHeaderView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.width / 2))
        tableHeaderView.imageView.kf.setImage(with: URL(string: venue.picture_url ?? ""))
        let isHotPick = venue.bat ?? 0
        isHotPick == 1 ? (tableHeaderView.hotPickView.isHidden = false) : (tableHeaderView.hotPickView.isHidden = true)
        
        tableView = UITableView()
        tableView.tableHeaderView = tableHeaderView
        tableView.backgroundColor = .groupTableViewBackground
        tableView.showsVerticalScrollIndicator = false
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.register(VenueGeneralInfoTableViewCell.self, forCellReuseIdentifier: "generalInfoCell")
        tableView.register(VenueSmokingInfoTableViewCell.self, forCellReuseIdentifier: "smokingInfoCell")
        tableView.register(VenueDescriptionTableViewCell.self, forCellReuseIdentifier: "descriptionInfoCell")
        tableView.register(VenueGalleryTableViewCell.self, forCellReuseIdentifier: "galleryCell")
        tableView.register(VenueTagGroupTableViewCell.self, forCellReuseIdentifier: "tagCell")
        tableView.register(VenueSecondaryTagTableViewCell.self, forCellReuseIdentifier: "secondaryTagCell")
        tableView.delegate = self
        tableView.dataSource = self
        
        workHoursView = WorkHoursView()
        workHoursView.alpha = 0
        workHoursView.delegate = self
        workHoursView.setupStackViewData(venue: venue)
        
        self.view.addSubview(workHoursView)
        self.view.addSubview(tableView)
        
        self.view.bringSubviewToFront(workHoursView)
    }
    
    //MARK:- Setting Up Constraints
    func setupConstraints() {
        tableView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalTo(self.view)
            make.top.equalTo(self.view.safeAreaLayoutGuide)
        }
        
        workHoursView.snp.makeConstraints { (make) in
            make.center.equalTo(self.view)
            make.width.equalTo(self.view).dividedBy(1.3)
            make.height.equalTo(self.view.snp.width).dividedBy(1.1)
        }
    }
}

//MARK:- WorkHoursView Delegate Methods
extension VenueDetailsViewController: WorkHoursViewDelegate {
    func okButtonTapped() {
        UIView.transition(with: workHoursView, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.workHoursView.alpha = 0
        })
    }
    
    func locationTapped() {
        if let latitude = venue.lat, let longitude = venue.lon {
            
            let regionDistance:CLLocationDistance = 10000
            let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
            let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
            let options = [
                MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
                MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
            ]
            let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
            let mapItem = MKMapItem(placemark: placemark)
            mapItem.name = "Place Name"
            mapItem.openInMaps(launchOptions: options)
        }
    }
}

//MARK:- UITableView Delegate And Datasource Methods
extension VenueDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == 0) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "generalInfoCell", for: indexPath) as! VenueGeneralInfoTableViewCell
            cell.setupCell(venue: venue)
            cell.delegate = self
            return cell
        } else if (indexPath.row == 1) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "smokingInfoCell", for: indexPath) as! VenueSmokingInfoTableViewCell
            cell.setupCell(venue: venue)
            return cell
        } else if (indexPath.row == 2) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "descriptionInfoCell", for: indexPath) as! VenueDescriptionTableViewCell
            cell.setupCell(venue: venue)
            return cell
        } else if (indexPath.row == 3) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "galleryCell", for: indexPath) as! VenueGalleryTableViewCell
            cell.setupCell(venue: venue)
            cell.delegate = self
            return cell
        } else if (indexPath.row == 4) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "tagCell", for: indexPath) as! VenueTagGroupTableViewCell
            cell.setupCell(venue: venue)
            cell.delegate = self
            return cell
        } else if (indexPath.row == 5) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "secondaryTagCell", for: indexPath) as! VenueSecondaryTagTableViewCell
            cell.setupCell(venue: venue)
            cell.delegate = self
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.row == 0) {
            return UITableView.automaticDimension
        } else if (indexPath.row == 1) {
            return self.view.frame.size.width/6
        } else if (indexPath.row == 3) {
            return self.view.frame.size.width/1.8
        } else if (indexPath.row == 5) {
            return UITableView.automaticDimension
        } else {
            return UITableView.automaticDimension
        }
    }
}

//MARK:- VenueGalleryTableViewCell Delegate Methods
extension VenueDetailsViewController: VenueGalleryTableViewCellDelegate {
    func galleryImageTappedWith(index: Int) {
        if let galleries = venue.gallery {
            let galleryVC = UINavigationController(rootViewController: GalleryViewController(galleries: galleries, selectedIndex: index))
            
            self.present(galleryVC, animated: true, completion: nil)
        }
    }
}

//MARK:- VenueGeneralInfoCell Delegate Methods
extension VenueDetailsViewController: VenueGeneralInfoTableViewCellDelegate {
    func workHourTapped() {
        UIView.transition(with: workHoursView, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.workHoursView.alpha = 1
        })
    }
}

//MARK:- VenueSecondaryTagTableViewCell Delegate Methods
extension VenueDetailsViewController: VenueSecondaryTagTableViewCellDelegate {
    func secondaryTagTappedWith(tagName: String) {
        var venuesGroupedByTag = [Venue]()
        venuesGroupedByTag = Utilities.sharedInstance.getSortedAndFilteredVenues(tagName: tagName)
        
        navigationController?.pushViewController(VenuesListViewController(venues: venuesGroupedByTag, tagName: tagName), animated: true)
    }
}

//MARK:- VenueSecondaryTagTableViewCell Delegate Methods
extension VenueDetailsViewController: VenueTagGroupTableViewCellDelegate {
    func primaryTagTappedWith(tagName: String) {
        var venuesGroupedByTag = [Venue]()
        venuesGroupedByTag = Utilities.sharedInstance.getSortedAndFilteredVenues(tagName: tagName)
        
        navigationController?.pushViewController(VenuesListViewController(venues: venuesGroupedByTag, tagName: tagName), animated: true)
    }
}
