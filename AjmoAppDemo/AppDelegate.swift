//
//  AppDelegate.swift
//  AjmoAppDemo
//
//  Created by Nenad Ljubik on 10/7/20.
//  Copyright © 2020 Nenad Ljubik. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var navigationController: UINavigationController!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        navigationController = UINavigationController(rootViewController: DiscoverViewController())
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        
        setImageOnNavigationBar()
        
        window?.rootViewController = navigationController//CalendarViewController()
        window?.makeKeyAndVisible()
        
        return true
    }
    
    //MARK:- Set Image On NavigationBar
    func setImageOnNavigationBar() {
        if let navigationBar = self.navigationController?.navigationBar {
            navigationBar.tintColor = .white
            let gradient = CAGradientLayer()
            var bounds = navigationBar.bounds
            bounds.size.height += UIApplication.shared.statusBarFrame.size.height
            gradient.frame = bounds
            gradient.colors = [Utilities.sharedInstance.colorFromHexCode(hex: "FFC75F").cgColor, Utilities.sharedInstance.colorFromHexCode(hex: "FF9671").cgColor]
            gradient.startPoint = CGPoint(x: 0, y: 0)
            gradient.endPoint = CGPoint(x: 1, y: 0)
            
            if let image = Utilities.sharedInstance.getImageFrom(gradientLayer: gradient) {
                navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
            }
        }
    }
}

